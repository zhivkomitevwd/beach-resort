import React, { Component } from 'react';
import { FaCocktail, FaHiking, FaShuttleVan, FaBeer } from 'react-icons/fa';

import '../css/services.css';
import Title from './Title';
import '../css/title.css';

class Services extends Component {
    constructor(props) {
        super(props);

        this.state = {
            services: [
                {
                    id: 1,
                    icon: <FaCocktail />,
                    title: 'Free Cocktails',
                    info:
                        'Find the latest and best cocktails in our bars, when you need a space to catch up with friends, family or coworkers',
                },
                {
                    id: 2,
                    icon: <FaHiking />,
                    title: 'Your Dream Hiking',
                    info:
                        'Your dream backpacking trips are waiting for you. Find and explore new hiking trails and backpacking routes across the country with advice, adventure travel stories, topo maps, photography, and more from our experts.',
                },
                {
                    id: 3,
                    icon: <FaShuttleVan />,
                    title: 'SuperShuttle',
                    info:
                        'Use SuperShuttle for convenience, price transparency, and affordability for rides to and from the airport. Book a ride today!',
                },
                {
                    id: 4,
                    icon: <FaBeer />,
                    title: 'Craft Beer',
                    info:
                        'If you’re a beer drinker, there are certain beers you have to drink. At least once. Ours is one of them.',
                },
            ],
        };
    }

    render() {
        return (
            <section className='services'>
                <Title title='services' />
                <div className='services-center'>
                    {this.state.services.map((item) => {
                        return (
                            <article key={item.id} className='service'>
                                <span>{item.icon}</span>
                                <h6>{item.title}</h6>
                                <p>{item.info}</p>
                            </article>
                        );
                    })}
                </div>
            </section>
        );
    }
}

export default Services;
