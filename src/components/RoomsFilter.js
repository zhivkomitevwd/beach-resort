import React, { useContext } from 'react';

import '../css/rooms-filter.css';
import { RoomContext } from '../Context';
import Title from './Title';

const getUniqueVal = (items, value) => {
    return [...new Set(items.map((item) => item[value]))];
};

const RoomsFilter = ({ rooms }) => {
    // console.log('rooms', rooms);
    const context = useContext(RoomContext);

    const {
        handleChange,
        type,
        capacity,
        price,
        minPrice,
        maxPrice,
        minSize,
        maxSize,
        breakfast,
        pets,
    } = context;

    // get unique types
    let types = getUniqueVal(rooms, 'type');
    types = ['all', ...types].map((type, i) => {
        return (
            <option value={type} key={i}>
                {type}
            </option>
        );
    });

    // guests capacity
    let guests = getUniqueVal(rooms, 'capacity');
    guests = guests.map((guest, i) => {
        return (
            <option value={guest} key={i}>
                {guest}
            </option>
        );
    });

    return (
        <section className='filter-container'>
            <Title title='search rooms' />
            <form className='filter-form'>
                {/* select type */}
                <div className='form-group'>
                    <label htmlFor='type'>room type</label>
                    <select
                        name='type'
                        id='type'
                        value={type}
                        onChange={handleChange}
                        className='form-control'
                    >
                        {types}
                    </select>
                </div>
                {/* end select type */}

                {/* guests */}
                <div className='form-group'>
                    <label htmlFor='capacity'>guests</label>
                    <select
                        name='capacity'
                        id='capacity'
                        value={capacity}
                        onChange={handleChange}
                        className='form-control'
                    >
                        {guests}
                    </select>
                </div>
                {/* end guests */}

                {/* room price */}
                <div className='form-group'>
                    <label htmlFor='price'>room price ${price}</label>
                    <input
                        type='range'
                        name='price'
                        min={minPrice}
                        max={maxPrice}
                        id='price'
                        value={price}
                        onChange={handleChange}
                        className='form-control'
                    />
                </div>
                {/* end room price */}

                {/* size */}
                <div className='form-group'>
                    <label htmlFor='size'>room size</label>
                    <div className='size-inputs'>
                        <input
                            type='number'
                            name='minSize'
                            id='size'
                            value={minSize}
                            onChange={handleChange}
                            className='size-input'
                        />
                        <input
                            type='number'
                            name='maxSize'
                            id='size'
                            value={maxSize}
                            onChange={handleChange}
                            className='size-input'
                        />
                    </div>
                </div>
                {/* end size */}

                {/* breakfast / pets */}
                <div className='form-group'>
                    <div className='single-extra'>
                        <input
                            type='checkbox'
                            name='breakfast'
                            id='breakfast'
                            checked={breakfast}
                            onChange={handleChange}
                        />
                        <label htmlFor='breakfast'>breakfast</label>
                    </div>
                    <div className='single-extra'>
                        <input
                            type='checkbox'
                            name='pets'
                            id='pets'
                            checked={pets}
                            onChange={handleChange}
                        />
                        <label htmlFor='pets'>pets</label>
                    </div>
                </div>
                {/* end breakfast / pets */}
            </form>
        </section>
    );
};

export default RoomsFilter;
